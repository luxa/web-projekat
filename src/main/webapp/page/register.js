
oroApp.controller('registerController', function ($scope, $http, $state, $mdDialog, authorizationService, FileUploader) {

    $scope.username = {
        exists: true
    };

    $scope.images = [];

    $scope.user = {};

    $scope.hasUrl = false;

    var uploader = $scope.uploader = new FileUploader({
        url: '/api/images/new/users'
    });

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        $scope.images[0] = fileItem;
        $scope.images[0].upload();
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        $scope.user.imageUrl = response;
        $scope.hasUrl = true;
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

    $scope.usernameChanged = function () {
        if ($scope.user !== undefined && $scope.user !== null) {
            $http.get('/api/users/exists/' + $scope.user.username).success(function () {
                $scope.username.exists = false;
            }).error(function () {
                $scope.username.exists = true;
            });
        }
    };

    $scope.register = function () {
        authorizationService.register($scope.user, function(data) {
            authorizationService.setUser(data);
            $state.transitionTo('home');
        }, onError);
    };

    $scope.goToSignIn = function () {
        $state.transitionTo('signIn');
    };

    var onError = function () {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .title('Greška pri registraciji')
                .content('Nešto nije prošlo kako treba, pokušajte ponovo.')
                .ok('Ok')
        );
    };

});
