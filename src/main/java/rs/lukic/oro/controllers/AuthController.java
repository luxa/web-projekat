package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import rs.lukic.oro.model.User;
import rs.lukic.oro.model.dto.UserDTO;
import rs.lukic.oro.repository.UserRepository;

import java.util.ArrayList;

@RestController
@RequestMapping("/api")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST, value = "/signIn")
    public ResponseEntity login(@RequestBody UserDTO userDTO) {
        final User user = userRepository.findByUsernameAndPassword(userDTO.getUsername(), userDTO.getPassword());

        if (user != null) {
            final java.util.Collection<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(user.getRole()));
            final Authentication authentication = new PreAuthenticatedAuthenticationToken(user, null, authorities);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    @ResponseBody
    public ResponseEntity register(@RequestBody UserDTO user) {
        final User u = userRepository.save(new User(user));

        return new ResponseEntity<>(u, HttpStatus.OK);
    }
}
