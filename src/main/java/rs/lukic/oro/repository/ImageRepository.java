package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.Image;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {

    Image findById(long id);

    List<Image> findByUrl(String url);

}
