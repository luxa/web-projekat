package rs.lukic.oro.model.dto;

import rs.lukic.oro.model.Review;

import java.util.Date;

public class ReviewDTO {

    private String comment;

    private int rating;

    private long userId;

    private long businessId;

    public ReviewDTO() {}

    public ReviewDTO(Review review) {
        this.comment = review.getComment();
        this.rating = review.getRating();
        this.userId = review.getUser().getId();
        this.businessId = review.getBusiness().getId();
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getBusinessId() {
        return businessId;
    }

    public void setBusinessId(long businessId) {
        this.businessId = businessId;
    }

    @Override
    public String toString() {
        return "ReviewDTO{" +
                "comment='" + comment + '\'' +
                ", rating=" + rating +
                ", userId=" + userId +
                ", businessId=" + businessId +
                '}';
    }
}
