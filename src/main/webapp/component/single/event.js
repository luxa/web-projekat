oroApp.controller('eventController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {
    $scope.page.current = -1;
    $scope.id = $stateParams.id;
    $scope.registered = {
        forEvent: false
    };

    $http.get('/api/events/single/' + $scope.id).success(function (data) {
        $scope.event = data;
        $scope.page.title = $scope.event.name;

        $http.get('/api/reservations/event/' + $scope.event.id).success(function (reservations) {
            $scope.reservations = reservations;
            for (var i in reservations) {
                if (reservations[i].user.id == $scope.user.id) {
                    $scope.registered.forEvent = true;
                }
            }
        });

    });

    $scope.checkEventCriteria = function () {
        if ($scope.user.id == $scope.event.user.id) {
            return true;
        }
        return Date.now() > $scope.event.applicationDate || $scope.registered.forEvent;
    };

    $scope.attendEvent = function () {
        $http.post('/api/reservations/insert', {userId: $scope.user.id, eventId: $scope.event.id}).success(function () {
            $http.get('/api/reservations/event/' + $scope.event.id).success(function (reservations) {
                $scope.reservations = reservations;
                for (var i in reservations) {
                    if (reservations[i].user.id == $scope.user.id) {
                        $scope.registered.forEvent = true;
                    }
                }
            });
        });
    };

});