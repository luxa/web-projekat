oroApp.controller('businessDialogController', function ($scope, $http, $state, $mdDialog, authorizationService, FileUploader, business, existed) {

    $scope.images = [];

    // vrati ovo na staro !
    $scope.hasUrl = existed;
    $scope.urlChanged = false;

    $scope.exists = {
        pib: false,
        accountNumber: false
    };

    $scope.existed = existed;

    $scope.business = existed ? {
        id: business.id,
        address: business.address,
        name: business.name,
        phone: business.phone,
        email: business.email,
        webSite: business.webSite,
        pib: business.pib,
        accountNumber: business.accountNumber,
        imageUrl: business.imageUrl,
        cityName: business.city.name,
        categoryId: business.category.id
    } : {};

    $http.get('/api/categories/all').success(function (data) {
        $scope.categories = data;
    });

    $scope.submit = function () {
        $scope.business.userId = authorizationService.getUser().id;
        $http.post('/api/businesses/insert', $scope.business).success(function () {
            $scope.close();
        }).error(function () {
            $scope.close();
        });
    };

    $scope.close = function () {
        $mdDialog.hide();
    };

    var onError = function () {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .title('Greška pri registraciji')
                .content('Nešto nije prošlo kako treba, pokušajte ponovo.')
                .ok('Ok')
        );
    };

    $scope.pibChanged = function () {
        $http.get('/api/businesses/pib/exists/' + $scope.business.pib).success(function () {
            $scope.exists.pib = false;
        }).error(function () {
            $scope.exists.pib = true;
        });
    };

    $scope.accountNumberChanged = function () {
        $http.get('/api/businesses/accountNumber/exists/' + $scope.business.accountNumber).success(function () {
            $scope.exists.accountNumber = false;
        }).error(function () {
            $scope.exists.accountNumber = true;
        });
    };


    var uploader = $scope.uploader = new FileUploader({
        url: '/api/images/new/objects'
    });

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        $scope.images[0] = fileItem;
        $scope.images[0].upload();
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        $scope.business.imageUrl = response;
        $scope.hasUrl = true;
        $scope.urlChanged = true;
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

});
