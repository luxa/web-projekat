package rs.lukic.oro.model;

import rs.lukic.oro.model.dto.BusinessDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "Business")
public class Business {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "address", nullable = false)
    @NotNull
    private String address;

    @ManyToOne
    @JoinColumn()
    @NotNull
    private City city;

    @ManyToOne
    @JoinColumn(name = "userId")
    @NotNull
    private User user;

    @Column(name = "name", nullable = false)
    @NotNull
    private String name;

    @Column(name = "phone", nullable = false)
    @NotNull
    private String phone;

    @Column(name = "email", nullable = false)
    @NotNull
    private String email;

    @Column(name = "webSite", nullable = false)
    @NotNull
    private String webSite;

    @Column(name = "pib", nullable = false, unique = true)
    @NotNull
    private String pib;

    @Column(name = "accountNumber", nullable = false, unique = true)
    @NotNull
    private String accountNumber;

    @Column(name = "imageUrl", nullable = false)
    @NotNull
    private String imageUrl;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "categoryId")
    private Category category;

    public Business() {
    }

    public Business(BusinessDTO businessDTO) {
        this.id = businessDTO.getId();
        this.address = businessDTO.getAddress();
        this.name = businessDTO.getName();
        this.phone = businessDTO.getPhone();
        this.email = businessDTO.getEmail();
        this.webSite = businessDTO.getWebSite();
        this.pib = businessDTO.getPib();
        this.accountNumber = businessDTO.getAccountNumber();
        this.imageUrl = businessDTO.getImageUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Business{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", city=" + city +
                ", user=" + user +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", webSite='" + webSite + '\'' +
                ", pib='" + pib + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", category=" + category +
                '}';
    }
}
