oroApp.controller('homeController', function ($scope, $state, $location, $log, $rootScope, $mdSidenav, $interval, authorizationService) {

    $scope.user = authorizationService.getUser();

    $scope.page = {
        title: 'ORO - Onlajn Recenzije Objekata',
        current: -1
    };

    $scope.logout = function () {
        $state.transitionTo('logout');
    };

    // side navigation stuff
    $scope.toggleSidenav = buildToggler('left');

    function buildToggler(navID) {
        return function () {
            $mdSidenav(navID).toggle();
        }
    }

    $scope.goToEvents = function () {
        $state.transitionTo('home.events');
        $mdSidenav('left').close();
    };

    $scope.goToBusinesses = function () {
        $state.transitionTo('home.businesses');
        $mdSidenav('left').close();
    };

    $scope.goToReviews = function () {
        $state.transitionTo('home.reviews');
        $mdSidenav('left').close();
    };

    $scope.goToMyEvents = function () {
        $state.transitionTo('home.myevents');
        $mdSidenav('left').close();
    };
});