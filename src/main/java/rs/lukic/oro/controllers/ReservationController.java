package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.lukic.oro.model.Reservation;
import rs.lukic.oro.model.dto.ReservationDTO;
import rs.lukic.oro.repository.EventRepository;
import rs.lukic.oro.repository.ReservationRepository;
import rs.lukic.oro.repository.UserRepository;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {

    @Autowired
    ReservationRepository reservationRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    EventRepository eventRepository;

    @RequestMapping(value = "/event/{eventId}", method = RequestMethod.GET)
    public ResponseEntity getByEvent(@PathVariable long eventId) {
        return new ResponseEntity<>(reservationRepository.findByEventId(eventId), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity getByUser(@PathVariable long userId) {
        return new ResponseEntity<>(reservationRepository.findByUserId(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity insertReservation(@RequestBody ReservationDTO reservationDTO) {
        final Reservation reservation = new Reservation();
        reservation.setUser(userRepository.findById(reservationDTO.getUserId()));
        reservation.setEvent(eventRepository.findById(reservationDTO.getEventId()));
        reservationRepository.save(reservation);

        return new ResponseEntity(HttpStatus.OK);
    }

}
