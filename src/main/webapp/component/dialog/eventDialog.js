oroApp.controller('eventDialogController', function ($scope, $http, $state, $mdDialog, authorizationService, FileUploader, user, business, selected) {

    $scope.images = [];
    $scope.user = user;

    $scope.hasUrl = false;
    $scope.business = business;

    $scope.event = selected ? {
        businessId: business.id,
        userId: $scope.user.id
    } : {
        userId: $scope.user.id
    };

    $http.get('/api/businesses/user/' + $scope.user.id).success(function (data) {
        $scope.businesses = data;
    });

    $scope.submit = function () {
        $scope.business.userId = authorizationService.getUser().id;
        $http.post('/api/events/insert', $scope.event).success(function () {
            $scope.close();
        }).error(function () {
            $scope.close();
        });
    };

    $scope.close = function () {
        $mdDialog.hide();
    };

    var uploader = $scope.uploader = new FileUploader({
        url: '/api/images/new/events'
    });

    uploader.onWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        $scope.images[0] = fileItem;
        $scope.images[0].upload();
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        $scope.event.imageUrl = response;
        $scope.hasUrl = true;
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };

});
