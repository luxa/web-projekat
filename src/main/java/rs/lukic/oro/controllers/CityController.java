package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.lukic.oro.model.City;
import rs.lukic.oro.repository.CityRepository;

import java.util.List;

@RestController
@RequestMapping("/api/cities")
public class CityController {

    @Autowired
    private CityRepository cityRepository;

    @RequestMapping(value = {"", "/all"}, method = RequestMethod.GET)
    public List<City> getAllCities() {
        return cityRepository.findAll();
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET)
    public City getCityById(@PathVariable long id) {
        return cityRepository.findById(id);
    }

    @RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
    public City getCityByName(@PathVariable String name) {
        return cityRepository.findByName(name);
    }

}
