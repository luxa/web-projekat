oroApp.controller('businessesController', function ($scope, $state, $mdDialog, $location, $log, $http, $rootScope, $mdSidenav, $interval, authorizationService) {

    $scope.businesses = [];
    $scope.page.title = 'Objekti';
    $scope.page.current = 1;

    $http.get('/api/businesses/all').success(function (data) {
        $scope.businesses = data;
    });

    $scope.showBusiness = function (id) {
        $state.transitionTo('home.business', {id: id});
    };

    $scope.search = function (business) {
        return (!$scope.query || (business.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1)
        || (business.city.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1)
        || (business.address.toLowerCase().indexOf($scope.query.toLowerCase()) != -1));

    };

    $scope.deleteBusiness = function (id) {
        $http.delete('api/businesses/delete/' + id).success(function () {
            $http.get('/api/businesses/all').success(function (data) {
                $scope.businesses = data;
            });
        });
    };

    $scope.editBusiness = function (business, existed) {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/businessDialog.html',
            controller: 'businessDialogController',
            locals: {
                business: business,
                existed: existed
            },
            onRemoving: function () {
                $http.get('/api/businesses/all').success(function (data) {
                    $scope.businesses = data;
                });
            }
        });
    };

    $scope.addEvent = function (business, selected) {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/eventDialog.html',
            controller: 'eventDialogController',
            locals: {
                user: $scope.user,
                business: business,
                selected: selected
            },
            onRemoving: function () {
                $http.get('/api/businesses/all').success(function (data) {
                    $scope.businesses = data;
                });
            }
        });
    };

    $scope.addBusiness = function () {
        $scope.editBusiness({}, false);
    };

});