package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.lukic.oro.model.Review;
import rs.lukic.oro.model.dto.ReviewDTO;
import rs.lukic.oro.repository.BusinessRepository;
import rs.lukic.oro.repository.ReviewRepository;
import rs.lukic.oro.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BusinessRepository businessRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity getAll() {
        return new ResponseEntity<>(reviewRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/business/{businessId}", method = RequestMethod.GET)
    public ResponseEntity getByBusinessId(@PathVariable long businessId) {
        return new ResponseEntity<>(reviewRepository.findByBusinessId(businessId), HttpStatus.OK);
    }

    @RequestMapping(value = "/business/{businessId}/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity getByBusinessAndUser(@PathVariable long businessId, @PathVariable long userId) {
        final Review review = reviewRepository.findByBusinessIdAndUserId(businessId, userId);
        if (review != null) {
            return new ResponseEntity<>(review, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/except/business/{businessId}/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity getByBusinessIdExceptUsers(@PathVariable long businessId, @PathVariable long userId) {
        final List<Review> reviews = reviewRepository.findByBusinessId(businessId);
        final List<Review> usersReviews = new ArrayList<>();

        for (Review review : reviews) {
            if (review.getUser().getId() == userId) {
                usersReviews.add(review);
            }
        }
        reviews.removeAll(usersReviews);

        return new ResponseEntity<>(reviews, HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteReview(@PathVariable long id) {
        reviewRepository.delete(id);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.PUT)
    public ResponseEntity addReview(@RequestBody ReviewDTO reviewDTO) {
        final Review review = new Review(reviewDTO);
        review.setBusiness(businessRepository.findById(reviewDTO.getBusinessId()));
        review.setUser(userRepository.findById(reviewDTO.getUserId()));

        reviewRepository.save(review);

        return new ResponseEntity<>(review, HttpStatus.OK);
    }

}
