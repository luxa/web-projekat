package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.Event;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {

    Event findById(long id);

    List<Event> findByBusinessId(long businessId);

    List<Event> findByUserId(long userId);

}
