oroApp.controller('businessController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {

    $scope.id = $stateParams.id;
    $scope.dataRetrieved = true;
    $scope.hasReview = false;
    $scope.usersReview = {};
    $scope.reviews = [];
    $scope.preview = 1;
    $scope.events = [];
    $scope.userReservations = [];

    $http.get('/api/businesses/single/' + $scope.id).success(function (business) {
        $scope.business = business;
        $scope.page.title = business.name;
        $scope.page.current = -1;

        // see if the current user has left a review
        if (business.id != $scope.user.id) {
            $http.get('/api/reviews/business/' + business.id + '/user/' + $scope.user.id).success(function (review) {
                $scope.hasReview = true;
                $scope.usersReview = review;
            });
        }
        $http.get('/api/reviews/except/business/' + $scope.business.id + '/user/' + $scope.user.id).success(function (reviews) {
            $scope.reviews = reviews;
        });

        $http.get('/api/events/business/' + business.id).success(function (data) {
            $scope.events = data;
        });

        $http.get('/api/reservations/user/' + $scope.user.id).success(function (data) {
            $scope.userReservations = data;
        })

    }).error(function () {
        $scope.dataRetrieved = false;
    });

    $scope.eventReserved = function(event) {
        if ($scope.user.id == event.user.id) {
            return true;
        }

        if (Date.now() > event.applicationDate) {
            return true;
        }

        for (var i in $scope.userReservations) {
            if (event.id == $scope.userReservations[i].event.id) {
                return true;
            }
        }
        return false;
    };

    $scope.deleteReview = function () {
        if ($scope.usersReview.id) {
            $http.delete('/api/reviews/delete/' + $scope.usersReview.id).success(function () {
                $scope.hasReview = false;
                $scope.usersReview = {};

                $http.get('/api/reviews/business/' + $scope.business.id).success(function (reviews) {
                    $scope.reviews = reviews;
                });
            });
        }
    };

    $scope.postReview = function () {
        $scope.usersReview.userId = $scope.user.id;
        $scope.usersReview.businessId = $scope.business.id;

        $http.put('/api/reviews/add', $scope.usersReview).success(function (review) {
            $scope.hasReview = true;
            $scope.usersReview = review;

            $http.get('/api/reviews/except/business/' + $scope.business.id + '/user/' + $scope.user.id).success(function (reviews) {
                $scope.reviews = reviews;
            });
        });
    };

    $scope.deleteBusiness = function () {
        $http.delete('api/businesses/delete/' + $scope.business.id).success(function () {
            $state.transitionTo('home.businesses');
        });
    };

    $scope.editBusiness = function () {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/businessDialog.html',
            controller: 'businessDialogController',
            locals: {
                business: $scope.business,
                existed: true
            },
            onRemoving: function () {
                $http.get('/api/businesses/single/' + $scope.business.id).success(function (data) {
                    $scope.business = data;
                });
            }
        });
    };

    $scope.addEvent = function () {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/eventDialog.html',
            controller: 'eventDialogController',
            locals: {
                user: $scope.user,
                business: $scope.business,
                selected: true
            },
            onRemoving: function () {
                $http.get('/api/events/business/' + $scope.business.id).success(function (data) {
                    $scope.events = data;
                });
            }
        });
    };

    $scope.showEvent = function (eventId) {
        $state.transitionTo('home.event', {id: eventId});
    };

    $scope.attendEvent = function(event) {
        $http.post('/api/reservations/insert', {userId: $scope.user.id, eventId: event.id}).success(function () {
            $http.get('/api/reservations/user/' + $scope.user.id).success(function (data) {
                $scope.userReservations = data;
            });
        });
    };


});