package rs.lukic.oro.model;

import rs.lukic.oro.model.dto.ReviewDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Review", uniqueConstraints = {@UniqueConstraint(columnNames = {"userId", "businessId"})})
public class Review {

    @Id
    @GeneratedValue
    private long id;

    @Column(name = "comment", nullable = false, length = 1000)
    @NotNull
    private String comment;

    @Column(name = "rating", nullable = false)
    @NotNull
    private int rating;

    @Column(name = "date", nullable = false)
    @NotNull
    private Date date;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne
    @JoinColumn(name = "businessId")
    @NotNull
    private Business business;

    public Review() {
        this.date = new Date();
    }

    public Review(ReviewDTO review) {
        this.comment = review.getComment();
        this.rating = review.getRating();
        this.date = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", rating=" + rating +
                ", date=" + date +
                ", user=" + user +
                ", business=" + business +
                '}';
    }
}
