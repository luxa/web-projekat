oroApp.controller('ownEventsController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {

    $scope.page.title = 'Moji događaji';
    $scope.page.current = 4;

    $scope.events = [];
    $scope.date = {};

    $http.get('/api/events/user/' + $scope.user.id).success(function(data) {
        $scope.events = data;
    });

    $scope.reset = function () {
        $scope.date = {};
        $scope.query = '';
    };

    $scope.search = function (event) {
        return (!$scope.query || event.business.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1 ||
            event.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1)
            && (!$scope.date.start || $scope.date.start <= event.applicationDate)
            && (!$scope.date.end || $scope.date.end >= event.applicationDate)
    };

    $scope.addEvent = function () {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/eventDialog.html',
            controller: 'eventDialogController',
            locals: {
                user: $scope.user,
                business: {},
                selected: false
            },
            onRemoving: function () {
                $http.get('/api/events/user/' + $scope.user.id).success(function (data) {
                    $scope.events = data;
                });
            }
        });
    };

    $scope.eventReserved = function(event) {
        return true;
    };

    $scope.showEvent = function (eventId) {
        $state.transitionTo('home.event', {id: eventId});
    };

});