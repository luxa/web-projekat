package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.Reservation;

import java.util.List;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    List<Reservation> findByEventId(long eventId);

    List<Reservation> findByUserId(long userId);

    Reservation findByEventIdAndUserId(long eventId, long userId);

}
