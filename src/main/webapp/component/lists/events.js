oroApp.controller('eventsController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {

    $scope.page.title = 'Događaji';
    $scope.page.current = 2;

    $scope.events = [];
    $scope.userReservations = [];
    $scope.date = {};

    $http.get('/api/events/all').success(function(data) {
        $scope.events = data;
    });

    $http.get('/api/reservations/user/' + $scope.user.id).success(function (data) {
        $scope.userReservations = data;
    });

    $scope.reset = function () {
        $scope.date = {};
        $scope.query = '';
    };

    $scope.search = function (event) {
        return (!$scope.query || event.business.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1 ||
            event.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1)
        && (!$scope.date.start || $scope.date.start <= event.applicationDate)
        && (!$scope.date.end || $scope.date.end >= event.applicationDate)
    };

    $scope.addEvent = function () {
        $mdDialog.show({
            parent: angular.element(document.body),
            templateUrl: 'component/dialog/eventDialog.html',
            controller: 'eventDialogController',
            locals: {
                user: $scope.user,
                business: {},
                selected: false
            },
            onRemoving: function () {
                $http.get('/api/events/business/' + $scope.business.id).success(function (data) {
                    $scope.events = data;
                });
            }
        });
    };

    $scope.eventReserved = function(event) {
        if ($scope.user.id == event.user.id) {
            return true;
        }

        if (Date.now() > event.applicationDate) {
            return true;
        }

        for (var i in $scope.userReservations) {
            if (event.id == $scope.userReservations[i].event.id) {
                return true;
            }
        }
        return false;
    };

    $scope.attendEvent = function(event) {
        $http.post('/api/reservations/insert', {userId: $scope.user.id, eventId: event.id}).success(function () {
            $http.get('/api/reservations/user/' + $scope.user.id).success(function (data) {
                $scope.userReservations = data;
            });
        });
    };

    $scope.showEvent = function (eventId) {
        $state.transitionTo('home.event', {id: eventId});
    };

});