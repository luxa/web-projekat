oroApp.controller('reviewsController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {
    $scope.page.current = 3;
    $scope.page.title = 'Recenzije';

    $http.get('/api/reviews/all').success(function (data) {
        $scope.reviews = data;
    });

    $scope.search = function (review) {
        return (!$scope.query || review.business.name.toLowerCase().indexOf($scope.query.toLowerCase()) != -1)
            && (!$scope.rating || review.rating == $scope.rating);
    };

    $scope.reset = function() {
        $scope.query = '';
        $scope.rating = undefined;
    }


});