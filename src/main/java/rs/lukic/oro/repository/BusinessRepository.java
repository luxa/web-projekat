package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.Business;

import java.util.List;

public interface BusinessRepository extends JpaRepository<Business, Long> {

    Business findById(long id);

    List<Business> findByUserId(long userId);

    Business findByPib(String pib);

    Business findByAccountNumber(String accountNumber);

}
