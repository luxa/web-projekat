oroApp.controller('signInController', function ($scope, $http, $state, $stateParams, $log, $mdDialog, authorizationService) {
    $scope.signIn = function () {
        authorizationService.signIn($scope.user, function (data) {
            authorizationService.setUser(data);
            $state.transitionTo('home.businesses');
        }, onError);
    };

    var onError = function () {
        $mdDialog.show(
            $mdDialog.alert()
                .parent(angular.element(document.body))
                .title('Greška pri logovanju')
                .content('Uneli ste pogrešno korisničko ime ili lozinku.')
                .ok('Ok')
        );
    };

    $scope.goToRegister = function () {
        $state.transitionTo('register');
    };
});