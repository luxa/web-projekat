package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

    User findById(long id);

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

}
