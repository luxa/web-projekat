package rs.lukic.oro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.lukic.oro.model.City;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {

    City findById(long id);

    City findByName(String name);

}
