'use strict';

var oroApp = angular.module('oroApp', ['ui.router', 'ngMessages', 'ngMaterial', 'angularFileUpload']);

oroApp.factory('authHttpResponseInterceptor', ['$q', '$location', function ($q, $location) {
    return {
        response: function (response) {
            if (response.status === 401) {
                console.log("Response 401");
            }
            return response || $q.when(response);
        },
        responseError: function (rejection) {
            if (rejection.status === 401) {
                console.log("Response Error 401", rejection);
                $location.path('/signIn');
            }
            return $q.reject(rejection);
        },
        request: function (config) {
            return config;
        }
    }
}]).config(function ($stateProvider, $urlRouterProvider, $httpProvider, $mdThemingProvider) {
    $mdThemingProvider.theme('default').accentPalette('purple');

    $httpProvider.interceptors.push('authHttpResponseInterceptor');
    $urlRouterProvider.otherwise('/signIn');
    $stateProvider
        .state('signIn', {
            url: '/signIn',
            controller: 'signInController',
            templateUrl: 'page/signIn.html',
            data: {
                pageTitle: 'Prijava'
            }
        })
        .state('register', {
            url: '/register',
            controller: 'registerController',
            templateUrl: 'page/register.html',
            data: {
                pageTitle: 'Registracija'
            }
        })
        .state('home', {
            url: '/home',
            abstract: true,
            controller: 'homeController',
            templateUrl: 'page/home.html',
            data: {
                pageTitle: 'ORO - home'
            }
        })
        .state('home.businesses', {
            url: '/businesses',
            templateUrl: 'component/lists/businesses.html',
            controller: 'businessesController'
        })
        .state('home.business', {
            url: '/business/{id:int}',
            templateUrl: 'component/single/business.html',
            controller: 'businessController'
        })
        .state('home.events', {
            url: '/events',
            templateUrl: 'component/lists/events.html',
            controller: 'eventsController'
        })
        .state('home.event', {
            url: '/event/{id:int}',
            templateUrl: 'component/single/event.html',
            controller: 'eventController'
        })
        .state('home.myevents', {
            url: '/my/events',
            templateUrl: 'component/lists/events.html',
            controller: 'ownEventsController'
        }).state('home.reviews', {
            url: '/reviews',
            templateUrl: 'component/lists/reviews.html',
            controller: 'reviewsController'
        })
        .state('logout', {
            url: '/logout',
            controller: function ($location, authorizationService) {
                authorizationService.removeUser();
                $location.path('/signIn');
            }
        });


    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}).run(function ($rootScope, $location, authorizationService) {
    var publicRoutes = ['/signIn', '/register'];

    var isPublicRoute = function (route) {
        for(var i in publicRoutes) {
            if (publicRoutes[i] === route) {
                return true;
            }
        }
        return false;
    };

    $rootScope.$on('$stateChangeStart', function (ev, to, toParams, from, fromParams) {
        if (isPublicRoute($location.path())) {
            return;
        }
        if (!isPublicRoute($location.path()) && !authorizationService.getUser()) {
            $location.path('/signIn');
        }
    });
});