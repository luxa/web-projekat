package rs.lukic.oro.model;

import rs.lukic.oro.model.dto.EventDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Event")
public class Event {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "applicationDate", nullable = false)
    @NotNull
    private Date applicationDate;

    @Column(name = "name", nullable = false)
    @NotNull
    private String name;

    @Column(name = "description", nullable = false)
    @NotNull
    private String description;

    @Column(name = "imageUrl", nullable = false)
    @NotNull
    private String imageUrl;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "businessId")
    private Business business;

    public Event() {}

    public Event(EventDTO eventDTO) {
        this.id = eventDTO.getId();
        this.applicationDate = eventDTO.getApplicationDate();
        this.name = eventDTO.getName();
        this.description = eventDTO.getDescription();
        this.imageUrl = eventDTO.getImageUrl();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getApplicationDate() {
        return applicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        this.applicationDate = applicationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", applicationDate=" + applicationDate +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", user=" + user +
                ", business=" + business +
                '}';
    }
}
