package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.lukic.oro.model.Business;
import rs.lukic.oro.model.City;
import rs.lukic.oro.model.Event;
import rs.lukic.oro.model.dto.BusinessDTO;
import rs.lukic.oro.repository.*;

import java.util.List;

@RestController
@RequestMapping("/api/businesses")
public class BusinessController {

    @Autowired
    BusinessRepository businessRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CityRepository cityRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    ReservationRepository reservationRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity getAll() {
        final List<Business> businesses = businessRepository.findAll();

        return new ResponseEntity<>(businesses, HttpStatus.OK);
    }

    @RequestMapping(value = "/single/{id}", method = RequestMethod.GET)
    public ResponseEntity getSingle(@PathVariable long id) {
        final Business business = businessRepository.findById(id);
        if (business != null) {
            return new ResponseEntity<>(business, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity findByUserId(@PathVariable long userId) {
        return new ResponseEntity<>(businessRepository.findByUserId(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable long id) {

        reviewRepository.delete(reviewRepository.findByBusinessId(id));

        final List<Event> events = eventRepository.findByBusinessId(id);
        for (Event event : events) {
            reservationRepository.delete(reservationRepository.findByEventId(event.getId()));
        }
        eventRepository.delete(events);

        businessRepository.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity addOrEdit(@RequestBody BusinessDTO businessDTO) {
        Business business = new Business(businessDTO);
        business.setUser(userRepository.findById(businessDTO.getUserId()));
        business.setCategory(categoryRepository.findById(businessDTO.getCategoryId()));
        City city = cityRepository.findByName(businessDTO.getCityName());

        if (city == null) {
            city = new City();
            city.setName(businessDTO.getCityName());
            city = cityRepository.save(city);
        }
        business.setCity(city);

        business = businessRepository.save(business);
        return new ResponseEntity<>(business, HttpStatus.OK);
    }

    @RequestMapping(value = "/pib/exists/{pib}", method = RequestMethod.GET)
    public ResponseEntity pibExists(@PathVariable String pib) {
        if (businessRepository.findByPib(pib) != null) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        } else {
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/accountNumber/exists/{accountNumber}", method = RequestMethod.GET)
    public ResponseEntity accountNumberExists(@PathVariable String accountNumber) {
        if (businessRepository.findByAccountNumber(accountNumber) != null) {
            return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
        } else {
            return new ResponseEntity(HttpStatus.OK);
        }
    }

}
