package rs.lukic.oro.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rs.lukic.oro.model.Event;
import rs.lukic.oro.model.dto.EventDTO;
import rs.lukic.oro.repository.BusinessRepository;
import rs.lukic.oro.repository.EventRepository;
import rs.lukic.oro.repository.UserRepository;

@RestController
@RequestMapping("/api/events")
public class EventController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BusinessRepository businessRepository;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity findAll() {
        return new ResponseEntity<>(eventRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(value = "/single/{eventId}", method = RequestMethod.GET)
    public ResponseEntity getOne(@PathVariable long eventId) {
        final Event event = eventRepository.findById(eventId);
        if (event != null) {
            return new ResponseEntity<>(event, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(value = "/business/{businessId}", method = RequestMethod.GET)
    public ResponseEntity findByBusiness(@PathVariable long businessId) {
        return new ResponseEntity<>(eventRepository.findByBusinessId(businessId), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ResponseEntity findByOwner(@PathVariable long userId) {
        return new ResponseEntity<>(eventRepository.findByUserId(userId), HttpStatus.OK);
    }

    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public ResponseEntity insert(@RequestBody EventDTO eventDTO) {
        final Event event = new Event(eventDTO);

        event.setUser(userRepository.findById(eventDTO.getUserId()));
        event.setBusiness(businessRepository.findById(eventDTO.getBusinessId()));

        eventRepository.save(event);

        return new ResponseEntity(HttpStatus.OK);
    }

}
